# Agriculture legislation
This is the sourcecode for a website that contains some laws related to agriculture in Peru.  

## Building
To build the website, `git clone` this repository and then, on your terminal, follow the commands
shown in the `gitlab-ci.yml` file. A virtual environment like `virtualenv` or `conda` is
recommended.  

## Tools
Some tools used in this repository:  

- Python
- Sphinx-doc with reStructuredText
- ReadTheDocs theme

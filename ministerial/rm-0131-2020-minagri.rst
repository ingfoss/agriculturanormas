Aprueban listado de procedimientos a cargo del Ministerio de Agricultura y Riego exceptuados de la suspensión del cómputo de plazos previsto en el D. U. N° 026-2020 y el D.U. N° 029-2020, formalizan implementación de correo institucional como un canal de recepción documentaria y dictan diversas disposiciones

============================================
RESOLUCIÓN MINISTERIAL Nro 0131-2020-MINAGRI
============================================

Lima, 4 de junio de 2020

VISTOS:

Los Memorandos N° 264-2020-MINAGRI-DVDIAR/DGAAA y 278-2020- MINAGRI-DVDIAR/DGAAA de la Dirección General de Asuntos Ambientales Agrarios, sobre aprobación del listado de procedimientos exceptuados de la suspensión del cómputo de plazos y los Informes Legales N° 347-2020-MINAGRI-SG/OGAJ y N° 366-MINAGRI-SG/OGAJ de la Oficina General de Asesoría Jurídica; y,

CONSIDERANDO:
=============

Considerando 1
~~~~~~~~~~~~~~

Que, conforme al artículo 3 del Decreto Legislativo N° 997, modificado por la Ley ,N° 30048, que aprueba la Ley de Organización y Funciones del Ministerio de Agricultura y Riego, éste tiene como ámbito de competencia las siguientes materias: tierras de uso agrícola y de pastoreo, tierras forestales y tierras eriazas con aptitud agraria; recursos forestales y su aprovechamiento; flora y fauna; recursos hídricos; infraestructura agraria; riego y utilización de agua para uso agrario; cultivos y crianzas; sanidad, investigación, extensión, transferencia de tecnología y otros servicios vinculados a la actividad agraria;

Considerando 2
~~~~~~~~~~~~~~

Que, mediante Decreto Supremo N° 008-2020-SA, se declaró la Emergencia Sanitaria a nivel nacional por el plazo de noventa (90) días calendario, y se dictaron medidas para la prevención y control para evitar la propagación del COVID-19;

Considerando 3
~~~~~~~~~~~~~~

Que, mediante Decreto Supremo N° 044-2020-PCM, publicado en el diario oficial El Peruano el 15 de marzo de 2020, precisado por los Decretos Supremos Nos. 045-2020-PCM y 046-2020-PCM, se declara el Estado de Emergencia Nacional por el plazo de quince (15) días calendario, y se dispone el aislamiento social obligatorio (cuarentena), por las circunstancias que afectan la vida de la Nación a consecuencia del brote del COVID-19; prorrogándose sucesivamente dicha medida por los Decretos Supremos Nos. 051-2020-PCM, 064-2020-PCM, 075-2020-PCM, 083-2020-PCM y 094-2020-PCM, hasta el 30 de junio de 2020;

Considerando 4
~~~~~~~~~~~~~~

Que, en el numeral 2 de la Segunda Disposición Complementaria Final del Decreto de Urgencia N° 026-2020, “Decreto de Urgencia que establece diversas medidas excepcionales y temporales para prevenir la propagación del coronavirus (COVID-19) en el territorio nacional”, se establece de manera excepcional, la suspensión por treinta (30) días hábiles, contados a partir del día siguiente de la publicación de dicho Decreto de Urgencia, del cómputo de los plazos de tramitación de los procedimientos administrativos sujetos a silencio positivo y negativo que se encuentren en trámite a la entrada en vigencia de dicha norma, con excepción de aquellos que cuenten con un pronunciamiento de la autoridad pendiente de notificación a los administrados; el indicado plazo fue prorrogado mediante los Decretos Supremos Nos. 076-2020-PCM y 087-2020-PCM, hasta el 10 de junio de 2020;

Considerando 5
~~~~~~~~~~~~~~

Que, por el artículo 28 del Decreto de Urgencia N° 029-2020, se declara la suspensión por treinta (30) días hábiles contados a partir del día siguiente de publicado dicho Decreto de Urgencia, del cómputo de los plazos de inicio y de tramitación de los procedimientos administrativos y procedimientos de cualquier índole, incluso los regulados por leyes y disposiciones especiales, que se encuentren sujetos a plazo, que se tramiten en entidades del Sector Público, y que no estén comprendidos en los alcances de la Segunda Disposición Complementaria Final del Decreto de Urgencia Nº 026-2020; incluyendo los que encuentran en trámite a la entrada en vigencia del indicado Decreto de Urgencia; el indicado plazo fue prorrogado por el artículo 12 del Decreto de Urgencia N° 053-2020, y Decreto Supremo N° 087-2020-PCM, hasta el 10 de junio del 2020;

Considerando 6
~~~~~~~~~~~~~~

Que, el numeral 12.2 del artículo 12 del Decreto de Urgencia N° 053-2020, faculta a las entidades públicas a aprobar mediante resolución de su titular, el listado de procedimientos cuya tramitación no se encuentra sujeta a la suspensión de plazos de tramitación de procedimientos administrativos establecida en el numeral 2 de la Segunda Disposición Complementaria Final del Decreto de Urgencia N° 026-2020 y sus prórrogas; y, a la suspensión del cómputo de plazos de inicio y tramitación de los procedimientos administrativos y procedimientos de cualquier índole establecida en el artículo 28 del Decreto de Urgencia N° 029-2020 y sus prórrogas, exceptuando los procedimientos iniciados de oficio;

Considerando 7
~~~~~~~~~~~~~~

Que, por Decreto Supremo Nº 080-2020-PCM, se aprueba la “Reanudación de Actividades” conforme a la estrategia elaborada por el Grupo de Trabajo Multisectorial conformado mediante la Resolución Ministerial Nº 144-2020-EF/15, la cual consta de cuatro (04) fases para su implementación, las que se irán evaluando permanentemente de conformidad con las recomendaciones de la Autoridad Nacional de Salud; y se establece que la Fase 1 de la “Reanudación de Actividades” se inicia en el mes de mayo del 2020;

Considerando 8
~~~~~~~~~~~~~~

Que, a través del Memorando de Vistos, sustentado en el Informe N° 002-2020-MINAGRI-DVDIAR-DGAAA-DGA-PTMDR, la Dirección General de Asuntos Ambientales Agrarios sustenta la necesidad de aprobar el listado de procedimientos administrativos cuya tramitación no se encuentra sujeta a la suspensión de plazos, ello, a fin de dar continuidad a dichos procedimientos;

Considerando 9
~~~~~~~~~~~~~~

Que, el numeral 20.4 del artículo 20 del TUO de la Ley Nº 27444, Ley del Procedimiento Administrativo General, aprobado por Decreto Supremo Nº 004-2019-JUS, establece que el administrado interesado o afectado por el acto que hubiera consignado en su escrito alguna dirección electrónica que conste en el expediente puede ser notificado a través de ese medio siempre que haya dado su autorización expresa para ello;

Considerando 10
~~~~~~~~~~~~~~~

Que, en consecuencia, es necesario aprobar el listado de procedimientos administrativos a cargo del Ministerio de Agricultura y Riego cuya tramitación no se encuentra sujeta a la suspensión de plazos de inicio y tramitación de procedimientos administrativos, conforme a lo establecido en el artículo 12 del Decreto de Urgencia Nº 053-2020 y el Decreto Supremo Nº 087-2020-PCM;

Con el visado del Despacho Viceministerial de Desarrollo e Infraestructura Agraria y Riego, de la Dirección General de Asuntos Ambientales Agrarios, de la Oficina General de Planeamiento y Presupuesto y de la Oficina General de Asesoría Jurídica; y,

De conformidad con el Decreto Legislativo N° 997, modificado por la Ley Nº 30048, que aprueba la Ley de Organización y Funciones del Ministerio de Agricultura y Riego, y su Reglamento de Organización y Funciones, aprobado por Decreto Supremo N° 008-2014-MINAGRI y modificatorias;

SE RESUELVE:
============

Artículo 1
~~~~~~~~~~

Aprobar el listado de procedimientos a cargo del Ministerio de Agricultura y Riego, exceptuados de la suspensión del cómputo de plazos previsto en el numeral 2 de la Segunda Disposición Complementaria Final del Decreto de Urgencia N° 026-2020 y en el artículo 28 del Decreto de Urgencia N° 029-2020, que se indica a continuación:

Denominación del procedimiento

1- Evaluación del estudio de levantamiento de suelos de competencia del Ministerio de Agricultura y Riego

2- Evaluación de Estudios de Impacto Ambiental Semidetallado (EIA-sd)

3- Evaluación de Informe de Gestión Ambiental (IGA)

4- Evaluación de Programa de Adecuación y Manejo Ambiental (PAMA)

5- Evaluación de Declaración Ambiental de Actividades en curso (DAAC)

6- Evaluación de Riesgo Ambiental para el Registro de Plaguicidas de uso agrario

7- Evaluación del Plan de Cierre y/o Abandono.

8- Evaluación de la modificación del Instrumento de Gestión Ambiental.

9- Plan de Cese Temporal

10- Evaluación del Informe Técnico para su conformidad.

11- Emisión de dictamen por adición de uso o modificación de dosis de uso de un producto PUA registrado.

Artículo 2
~~~~~~~~~~

Formalizar la implementación del correo institucional mesadepartes@minagri.gob.pe, como un canal de recepción documentaria, administrado por la Oficina de Atención a la Ciudadanía y Gestión Documentaria, al cual se accede desde el Portal Institucional del MINAGRI (www.gob.pe/minagri) o directamente desde el correo electrónico del administrado.

Artículo 3
~~~~~~~~~~

Para la notificación de las actuaciones y/o pronunciamiento del MINAGRI, se requerirá la autorización expresa del administrado para la notificación electrónica, la que será comunicada al siguiente correo electrónico institucional: mesadepartes@minagri.gob.pe. Los procedimientos administrativos en los que el administrado no haya prestado consentimiento para la notificación electrónica, continuarán suspendidos los plazos por el periodo establecido en el Decreto de Urgencia N° 026-2020 y sus prórrogas.

Artículo 4
~~~~~~~~~~

El cómputo de los plazos de inicio y tramitación de los procedimientos suspendidos a mérito de lo dispuesto en el Decreto de Urgencia N° 026-2020 y Decreto de Urgencia N° 029-2020, a los que se hace referencia el artículo 1 precedente, se reanudan a partir del día siguiente de publicada la presente Resolución en el Diario Oficial El Peruano.

Artículo 5
~~~~~~~~~~

Publicar la presente Resolución en el diario oficial El Peruano y en el Portal Institucional del Ministerio de Agricultura y Riego - MINAGRI (www.gob.pe/minagri).

Regístrese, comuníquese y publíquese.

JORGE LUIS MONTENEGRO CHAVESTA

Ministro de Agricultura y Riego

1867374-1
